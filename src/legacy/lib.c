unsigned int arr[16 * 15 * 14 * 13 * 12 * 11][2];
unsigned int (*ptr)[2];
inline unsigned int tobits(unsigned int i)
{
    return (1u << (i & 0xF))
    | (1u << (i >> 4 & 0xF))
    | (1u << (i >> 8 & 0xF))
    | (1u << (i >> 12 & 0xF))
    | (1u << (i >> 16 & 0xF))
    | (1u << (i >> 20 & 0xF));
}

void init()
{
    ptr = arr;
    for (int i = 0; i < 0x1000000; ++i)
    {
        unsigned int bits = tobits(i);
        if (__builtin_popcount(bits) == 6)
        {
            ptr[0][0] = i;
            ptr[0][1] = bits;
            ++ptr;
        }
    }
}

int bnc(unsigned int guess)
{
    unsigned int numpossible[56] = { 0, };
    const unsigned int bits = tobits(guess);
    guess = ~guess;

    for (unsigned int (*i)[2] = arr; i < ptr; ++i)
    {
        unsigned int strike = i[0][0] ^ guess;
        strike &= strike >> 1;
        ++numpossible[((short*)i)[3] = __builtin_popcount(strike & (strike >> 2) & 0x111111) * 8 + __builtin_popcount(i[0][1] & bits)];
    }

    int argmax = 0;
    for (int i = 1; i < 55; ++i)
        if (numpossible[i] > numpossible[argmax])
            argmax = i;

    unsigned int(*newptr)[2] = arr;
    for (unsigned int(*i)[2] = arr; i < ptr; ++i)
        if (i[0][1] >> 16 == argmax)
            *((long long*)newptr++) = *((long long *)i);
    ptr = newptr;

    return argmax;
}