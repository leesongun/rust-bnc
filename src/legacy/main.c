#include <stdio.h>
#include "lib.h"

int main(void)
{
    init();
    puts("Guess the number!");
    for (;;)
    {
        unsigned int x;
        puts("Please input your guess.");
        scanf("%6x", &x);
        int temp = bnc(x);
        printf("%d bulls, %d cows\n", temp / 9, temp % 9);
    }
}