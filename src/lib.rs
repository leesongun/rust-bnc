#![no_std]
mod test;
/// Pre-computed data for the fast calculation of bulls and cows result.
#[derive(Clone, Copy)]
pub struct Data(u32, u16);
impl Data {
    /// Creates a new `Data` from the given number.
    /// ```
    /// assert!(bnc::Data::new(0x000000).is_none());
    /// assert!(bnc::Data::new(0x012345).is_some());
    /// ```
    pub fn new(x: u32) -> Option<Self> {
        let bits = (0..6)
            .map(|i| 1_u16 << (x >> (4 * i) & 0xF))
            .fold(0, core::ops::BitOr::bitor);
        match bits.count_ones() {
            6 => Some(Self(x, bits)),
            _ => None,
        }
    }
    /// Returns the number stored in `self`.
    /// ```
    /// # let i = 0x012345;
    /// assert_eq!(bnc::Data::new(i).unwrap().get(), i);
    /// ```
    pub const fn get(self) -> u32 {
        self.0
    }
    /// Compares two `Data`s, and returns bulls and cows `Result`.
    pub const fn compare(self, other: Self) -> Result {
        let b = self.0 ^ !other.0;
        let b = b & b >> 1;
        Result(((b & b >> 2 & 0x11_1111).count_ones() * 8 + (self.1 & other.1).count_ones()) as u8)
    }
}

/// Bulls and Cows result.
#[derive(Copy, Clone, PartialEq)]
pub struct Result(u8);
impl Eq for Result {}
impl core::fmt::Display for Result {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        //write!(f, "{}🐂, {}🐄", self.get().0, self.get().1)
        write!(f, "{} bulls, {} cows", self.get().0, self.get().1)
    }
}

impl Result {
    /// All the possible values of `Self`.
    pub const VALUES: [Self; 27] = [
        Self(0),
        Self(1),
        Self(2),
        Self(3),
        Self(4),
        Self(5),
        Self(6),
        Self(9),
        Self(10),
        Self(11),
        Self(12),
        Self(13),
        Self(14),
        Self(18),
        Self(19),
        Self(20),
        Self(21),
        Self(22),
        Self(27),
        Self(28),
        Self(29),
        Self(30),
        Self(36),
        Self(37),
        Self(38),
        Self(45),
        Self(54),
    ];
    /// Creates a new `Result` from given numbers of bulls and cows.
    /// ```
    /// assert!(bnc::Result::new(7,0).is_none());
    /// assert!(bnc::Result::new(0,7).is_none());
    /// assert!(bnc::Result::new(4,3).is_none());
    /// assert!(bnc::Result::new(5,1).is_none());
    /// assert!(bnc::Result::new(1,5).is_some());
    /// assert!(bnc::Result::new(0,0).is_some());
    /// assert!(bnc::Result::new(0,6).is_some());
    /// assert!(bnc::Result::new(6,0).is_some());
    /// ```
    pub fn new(bulls: u8, cows: u8) -> Option<Self> {
        if bulls.saturating_add(cows) <= 6 && (bulls != 5 || cows != 1) {
            Some(Self(bulls * 9 + cows))
        } else {
            None
        }
    }
    /// Returns the `(bulls, cows)` of `self`.
    /// ```
    /// # for i in 0..=6{
    /// # for j in 0..=6{
    /// # if let Some(x) = bnc::Result::new(i,j){
    /// assert_eq!(bnc::Result::new(i,j).unwrap().get(), (i,j));
    /// # }}}
    /// ```
    pub const fn get(self) -> (u8, u8) {
        (self.0 >> 3, (self.0 & 7) - (self.0 >> 3))
    }
}

//I actually wanted to eliminate the need of new type definition
//using hashmap and implementing identity hash
//#[derive(Debug, Clone, PartialEq, Default, Hash)]
//enum-map does not work, it is more like an enum-array
/// An array indexed by `Result`.
pub struct ResultArray<T>([T; 55]);
//this is only a temporary solution
impl<T> Default for ResultArray<T>
where
    T: Default + Copy,
{
    fn default() -> Self {
        Self([T::default(); 55])
    }
}
impl<T> core::ops::Index<Result> for ResultArray<T> {
    type Output = T;
    fn index(&self, r: Result) -> &Self::Output {
        unsafe { self.0.get_unchecked(r.0 as usize) }
    }
}
impl<T> core::ops::IndexMut<Result> for ResultArray<T> {
    fn index_mut(&mut self, r: Result) -> &mut Self::Output {
        unsafe { self.0.get_unchecked_mut(r.0 as usize) }
    }
}
