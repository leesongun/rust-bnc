fn main() {
    let mut vec: std::vec::Vec<_> = (0..0x100_0000).filter_map(bnc::Data::new).collect();
    println!("Guess the number!");
    loop {
        println!("Please input your guess.");

        let mut guess = String::new();
        std::io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess = if let Ok(num) = u32::from_str_radix(guess.trim(), 16) {
            num
        } else {
            println!("Please enter a hexadecimal number.");
            continue;
        };

        let guess = if let Some(num) = bnc::Data::new(guess) {
            num
        } else {
            println!("The six digits must be all different.");
            continue;
        };

        let mut x = bnc::ResultArray::<u32>::default();
        for i in vec.iter().map(|x| guess.compare(*x)) {
            x[i] += 1;
        }
        let r = bnc::Result::VALUES
            .iter()
            .cloned()
            .min_by_key(|i| !x[*i])
            .unwrap();
        println!("{}", r);
        vec.retain(|x| guess.compare(*x) == r);

        if r.get() == (6, 0) {
            break;
        }
    }
}
