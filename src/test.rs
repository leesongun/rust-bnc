#![cfg(test)]
use super::*;

#[test]
fn data_check() {
    let init = (0..0x100_0000).filter_map(Data::new);
    assert_eq!(init.count(), (11..17).product());
}

#[test]
fn result_check() {
    let test_cases = [
        (0xAB_CDEF, 0, 0),
        (0x0A_BCDE, 1, 0),
        (0xAB_CDE0, 0, 1),
        (0xAB_0145, 2, 2),
        (0x01_2345, 6, 0),
        (0x12_3450, 0, 6),
    ];
    let anything = Data::new(0x01_2345).unwrap();
    for i in &test_cases {
        let temp = Data::new(i.0).unwrap().compare(anything);
        assert_eq!(temp.get(), (i.1, i.2));
    }
}

#[test]
fn values_test() {
    assert!((0..64)
        .filter_map(|x| Result::new(x / 8, x % 8))
        .eq(Result::VALUES.iter().cloned()))
}
